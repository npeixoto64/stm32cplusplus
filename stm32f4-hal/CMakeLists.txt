target_sources(${EXECUTABLE}
    PRIVATE
        src/stm32f4xx_hal_cortex.c
        src/stm32f4xx_hal_rcc.c
        src/stm32f4xx_hal.c
        src/stm32f4xx_hal_gpio.c
        src/stm32f4xx_hal_tim.c
        src/stm32f4xx_hal_tim_ex.c
)

target_include_directories(${EXECUTABLE}
    PRIVATE
        hdr
)
