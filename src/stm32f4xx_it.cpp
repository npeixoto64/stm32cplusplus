 extern "C"
 {
    #include "stm32f4xx.h"
    #include "gpio.h"

    using namespace mcal;

    extern TIM_HandleTypeDef htim4;

    extern Gpio led_d12;

    void __attribute__ ((section(".after_vectors"))) SysTick_Handler(void)
    {
        HAL_IncTick();
        if ( 0 == (HAL_GetTick()%1000))
            led_d12.Toggle();
        return;
    }

    void __attribute__ ((section(".after_vectors"))) TIM4_IRQHandler(void)
    {
        HAL_TIM_IRQHandler(&htim4);
        return;
    }
}
