#include "stm32f4xx.h"
#include "gpio.h"

using namespace mcal;

TIM_HandleTypeDef htim4;

Gpio dout_c5(gpio::Port::c, gpio::Pin::p5);
Gpio led_d12(gpio::Port::d, gpio::Pin::p12);
Gpio led_d15(gpio::Port::d, gpio::Pin::p15);
Gpio led_d13(gpio::Port::d, gpio::Pin::p13);
Gpio led_d14(gpio::Port::d, gpio::Pin::p14);

void InitLeds(void);
void InitTimer(void);

int main()
{
    HAL_Init();
    InitLeds();
    InitTimer();

    while (1)
    {
    }

    return 0;
}

void InitLeds(void)
{
    led_d15.Init
    (
        gpio::Mode::output_push_pull,
        gpio::Speed::low,
        gpio::Pull::no
    );
    dout_c5.Init
    (
        gpio::Mode::output_push_pull,
        gpio::Speed::low,
        gpio::Pull::no
    );
    led_d12.Init
    (
        gpio::Mode::output_push_pull,
        gpio::Speed::low,
        gpio::Pull::no
    );
    led_d13.Init(
        gpio::Mode::output_push_pull,
        gpio::Speed::low,
        gpio::Pull::no
    );
    led_d14.Init(
        gpio::Mode::output_push_pull,
        gpio::Speed::low,
        gpio::Pull::no
    );

    dout_c5.SetOFF();
    led_d12.SetOFF();
    led_d13.SetON();
    led_d15.SetON();
    led_d14.SetON();
}

void InitTimer(void)
{
    htim4.Instance = TIM4;
    htim4.Init.Prescaler = 21000U;
    htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
    htim4.Init.Period = 1U;
    htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
    htim4.Init.RepetitionCounter = 0; // Doesn't exist for TIM4

    HAL_TIM_Base_Init(&htim4);
    HAL_TIM_Base_Start_IT(&htim4);
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
    // Prevent unused argument(s) compilation warning
    UNUSED(htim);
    dout_c5.Toggle();
}
