#include "stdint.h"

namespace mcal
{
    namespace gpio
    {
        constexpr uint32_t periph_base = 0x40000000U;   // Peripheral base address in the alias region
        constexpr uint32_t ahb1periph_base = {periph_base + 0x00020000U};

        enum class Port : uint32_t
        {
            a = ahb1periph_base + 0x0000U,
            b = ahb1periph_base + 0x0400U,
            c = ahb1periph_base + 0x0800U,
            d = ahb1periph_base + 0x0C00U,
            e = ahb1periph_base + 0x1000U,
            f = ahb1periph_base + 0x1400U,
            g = ahb1periph_base + 0x1800U,
            h = ahb1periph_base + 0x1C00U,
            i = ahb1periph_base + 0x2000U
        };

        enum class Pin : uint16_t
        {
            p0 = 1U<<0,
            p1 = 1U<<1,
            p2 = 1U<<2,
            p3 = 1U<<3,
            p4 = 1U<<4,
            p5 = 1U<<5,
            p6 = 1U<<6,
            p7 = 1U<<7,
            p8 = 1U<<8,
            p9 = 1U<<9,
            p10 = 1U<<10,
            p11 = 1U<<11,
            p12 = 1U<<12,
            p13 = 1U<<13,
            p14 = 1U<<14,
            p15 = 1U<<15
        };

        enum class Ctrl : uint32_t
        {
            moder = 0x00U,
            otyper = 0x04U,
            ospeedr = 0x08U,
            pupdr = 0x0CU,
            idr = 0x10U,
            odr = 0x14U,
            bsrr = 0x18U,
            lckr = 0x1CU,
            afr_0 = 0x20U,
            afr_1 = 0x24U
        };

        enum class Mode : uint32_t
        {
            input = 0x00000000U,                // Input Floating Mode
            output_push_pull = 0x00000001U,     // Output Push Pull Mode
            output_open_drain = 0x00000011U,    // Output Open Drain Mode
            alt_func_push_pull = 0x00000002U,   // Alternate Function Push Pull Mode
            alt_func_open_drain = 0x00000012U,  // Alternate Function Open Drain Mode

            analog = 0x00000003U,               // Analog Mode
            
            it_rising = 0x10110000U,            // External Interrupt Mode with Rising edge trigger detection
            it_falling = 0x10210000U,           // External Interrupt Mode with Falling edge trigger detection
            it_raising_falling = 0x10310000U,   // External Interrupt Mode with Rising/Falling edge trigger detection

            evt_rising = 0x10120000U,           // External Event Mode with Rising edge trigger detection
            evt_falling = 0x10220000U,          // External Event Mode with Falling edge trigger detection
            evt_raising_falling = 0x10320000U   // External Event Mode with Rising/Falling edge trigger detection
        };

        enum class Speed : uint32_t
        {
            low = 0x00000000U,                  // IO works at 2 MHz, please refer to the product datasheet
            medium = 0x00000001U,               // range 12,5 MHz to 50 MHz, please refer to the product datasheet
            high = 0x00000002U,                 // range 25 MHz to 100 MHz, please refer to the product datasheet
            veryhigh = 0x00000003U              // range 50 MHz to 200 MHz, please refer to the product datasheet
        };

        enum class Pull : uint32_t
        {
            no = 0x00000000U,                   // No Pull-up or Pull-down activation
            up = 0x00000001U,                   // Pull-up activation
            down = 0x00000002U                  // Pull-down activation
        };
    }

    class Gpio
    {
        private:
            const gpio::Port m_port;
            const gpio::Pin m_pin;

        public:
            Gpio(const gpio::Port port, const gpio::Pin pin);
            void Init(gpio::Mode mode, gpio::Speed speed, gpio::Pull pull) const;
            inline void SetON(void) const
            {
                *reinterpret_cast<volatile uint32_t *>
                    (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::bsrr)) = static_cast<uint16_t>(m_pin);
            }
            inline void SetOFF(void) const
            {
                *reinterpret_cast<volatile uint32_t *>
                    (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::bsrr)) = static_cast<uint32_t>(m_pin) << 16U;
            }
            inline void Toggle(void) const
            {
                *reinterpret_cast<volatile uint32_t *>
                    (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::odr)) ^= static_cast<uint16_t>(m_pin);
            }
    };
}
