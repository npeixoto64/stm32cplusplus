#include "gpio.h"

using namespace mcal;

Gpio::Gpio(const gpio::Port port, const gpio::Pin pin) : m_port(port), m_pin(pin)
{

}

void Gpio::Init(gpio::Mode mode, gpio::Speed speed, gpio::Pull pull) const
{
    constexpr uint32_t PINS_NUMBER = 16;
    constexpr uint32_t GPIO_MODE = 0x00000003U;
    constexpr uint32_t EXTI_MODE = 0x10000000U;
    constexpr uint32_t GPIO_MODE_IT = 0x00010000U;
    constexpr uint32_t GPIO_MODE_EVT = 0x00020000U;
    constexpr uint32_t RISING_EDGE = 0x00100000U;
    constexpr uint32_t FALLING_EDGE = 0x00200000U;
    constexpr uint32_t GPIO_OUTPUT_TYPE = 0x00000010U;
    constexpr uint32_t GPIO_MODER_MODER0 = 0x00000003U;
    constexpr uint32_t GPIO_OSPEEDER_OSPEEDR0 = 0x00000003U;
    constexpr uint32_t GPIO_OTYPER_OT_0 = 0x00000001U;
    constexpr uint32_t GPIO_PUPDR_PUPDR0 = 0x00000003U;

    uint32_t position;
    uint32_t ioposition = 0x00U;
    uint32_t iocurrent = 0x00U;
    uint32_t temp = 0x00U;

    // Configure the port pins
    for(position = 0U; position < PINS_NUMBER; position++)
    {
        // Get the IO position
        ioposition = ((uint32_t)0x01U) << position;
        // Get the current IO position
        iocurrent = static_cast<uint32_t>(m_pin) & ioposition;

        if(iocurrent == ioposition)
        {
            //--------------------- GPIO Mode Configuration ------------------------
            // In case of Alternate function mode selection
            // if((mode == gpio::Mode::alt_func_push_pull) || (mode == gpio::Mode::alt_func_open_drain))
            // {
            //     // Configure Alternate function mapped with the current IO
            //     temp = GPIOx->AFR[position >> 3U];
            //     temp &= ~((uint32_t)0xFU << ((uint32_t)(position & (uint32_t)0x07U) * 4U)) ;
            //     temp |= ((uint32_t)(GPIO_Init->Alternate) << (((uint32_t)position & (uint32_t)0x07U) * 4U));
            //     GPIOx->AFR[position >> 3U] = temp;
            // }

            /* Configure IO Direction mode (Input, Output, Alternate or Analog) */
            temp = *reinterpret_cast<volatile uint32_t *>
                    (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::moder));
            temp &= ~(GPIO_MODER_MODER0 << (position * 2U));
            temp |= ((static_cast<uint32_t>(mode) & GPIO_MODE) << (position * 2U));
            *reinterpret_cast<volatile uint32_t *>
                    (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::moder)) = temp;

            /* In case of Output or Alternate function mode selection */
            if((mode == gpio::Mode::output_push_pull) || (mode == gpio::Mode::alt_func_push_pull) ||
               (mode == gpio::Mode::output_open_drain) || (mode == gpio::Mode::alt_func_open_drain))
            {
                /* Configure the IO Speed */
                temp = *reinterpret_cast<volatile uint32_t *>
                        (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::ospeedr));
                temp &= ~(GPIO_OSPEEDER_OSPEEDR0 << (position * 2U));
                temp |= (static_cast<uint32_t>(speed) << (position * 2U));
                *reinterpret_cast<volatile uint32_t *>
                    (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::ospeedr)) = temp;

                /* Configure the IO Output Type */
                temp = *reinterpret_cast<volatile uint32_t *>
                        (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::otyper));
                temp &= ~(GPIO_OTYPER_OT_0 << position) ;
                temp |= (((static_cast<uint32_t>(mode) & GPIO_OUTPUT_TYPE) >> 4U) << position);
                *reinterpret_cast<volatile uint32_t *>
                    (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::otyper)) = temp;
            }

            /* Activate the Pull-up or Pull down resistor for the current IO */
            temp = *reinterpret_cast<volatile uint32_t *>
                        (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::pupdr));
            temp &= ~(GPIO_PUPDR_PUPDR0 << (position * 2U));
            temp |= (static_cast<uint32_t>(pull) << (position * 2U));
            *reinterpret_cast<volatile uint32_t *>
                (static_cast<uint32_t>(m_port) + static_cast<uint32_t>(gpio::Ctrl::pupdr)) = temp;

            // /*--------------------- EXTI Mode Configuration ------------------------*/
            // /* Configure the External Interrupt or event for the current IO */
            // if((GPIO_Init->Mode & EXTI_MODE) == EXTI_MODE)
            // {
            //     /* Enable SYSCFG Clock */
            //     __HAL_RCC_SYSCFG_CLK_ENABLE();

            //     temp = SYSCFG->EXTICR[position >> 2U];
            //     temp &= ~(((uint32_t)0x0FU) << (4U * (position & 0x03U)));
            //     temp |= ((uint32_t)(GPIO_GET_INDEX(GPIOx)) << (4U * (position & 0x03U)));
            //     SYSCFG->EXTICR[position >> 2U] = temp;

            //     /* Clear EXTI line configuration */
            //     temp = EXTI->IMR;
            //     temp &= ~((uint32_t)iocurrent);
            //     if((GPIO_Init->Mode & GPIO_MODE_IT) == GPIO_MODE_IT)
            //     {
            //         temp |= iocurrent;
            //     }
            //     EXTI->IMR = temp;

            //     temp = EXTI->EMR;
            //     temp &= ~((uint32_t)iocurrent);
            //     if((GPIO_Init->Mode & GPIO_MODE_EVT) == GPIO_MODE_EVT)
            //     {
            //         temp |= iocurrent;
            //     }
            //     EXTI->EMR = temp;

            //     /* Clear Rising Falling edge configuration */
            //     temp = EXTI->RTSR;
            //     temp &= ~((uint32_t)iocurrent);
            //     if((GPIO_Init->Mode & RISING_EDGE) == RISING_EDGE)
            //     {
            //         temp |= iocurrent;
            //     }
            //     EXTI->RTSR = temp;

            //     temp = EXTI->FTSR;
            //     temp &= ~((uint32_t)iocurrent);
            //     if((GPIO_Init->Mode & FALLING_EDGE) == FALLING_EDGE)
            //     {
            //         temp |= iocurrent;
            //     }
            //     EXTI->FTSR = temp;
            // }
        }
    }
}
